import React, { Fragment, useEffect } from 'react';
import './App.css';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import { TextEditor } from './features/textEditor/TextEditor';
import { useAppDispatch } from './app/hooks';
import setAuthToken from './utils/setAuthToken';
import { Landing } from './features/landing/Landing';
import NavBar from './features/navbar/Navbar';
import { v4 as uuidv4 } from 'uuid';
import Login from './features/user/Login';
import Register from './features/user/Register';
import { Dashboard } from './features/dashboard/Dashboard';
import { authAsync } from './features/user/authSlice';
import { PrivateRoute } from './utils/PrivateRoute';

function App() {
  const dispatch = useAppDispatch();
  useEffect(() => {
    // check for token in LS when app first runs
    if (localStorage.token) {
      // if there is a token set axios headers for all requests
      setAuthToken(localStorage.token);
    }
    // try to fetch a user, if no token or invalid token we
    // will get a 401 response from our API
    dispatch(authAsync(localStorage.token));

    // log user out from all tabs if they log out in one tab
    //   window.addEventListener('storage', () => {
    //     if (!localStorage.token) {
    //       dispatch(logOut(''));
    //     }
    //   });

    // eslint-disable-next-line
  }, []);

  return (
    <BrowserRouter>
      <Fragment>
        <NavBar />
        <Routes>
          <Route element={<PrivateRoute />}>
            <Route path='/dashboard' element={<Dashboard />} />
            <Route
              path='/createDocument'
              element={<Navigate to={`/document/${uuidv4()}`} />}
            />
            <Route path='/document/:id' element={<TextEditor />} />
          </Route>
          <Route path='/' element={<Landing />} />
          <Route path='/login' element={<Login />} />
          <Route path='/register' element={<Register />} />
        </Routes>
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
