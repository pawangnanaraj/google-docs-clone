import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import socketReducer from '../features/textEditor/setSocketSplice';
import quillReducer from '../features/textEditor/setQuillSplice';
import authReducer from '../features/user/authSlice';
import allDocsReducer from '../features/dashboard/docsSplice';
import allCommentsReducer from '../features/textEditor/commentSplice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    socket: socketReducer,
    quill: quillReducer,
    allDocs: allDocsReducer,
    allComments: allCommentsReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
