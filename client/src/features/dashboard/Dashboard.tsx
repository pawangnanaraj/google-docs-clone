import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppSelector } from '../../app/hooks';
import './dashboard.css';
import { DocList } from './DocList';
import Spinner from '../../utils/Spinner';
import { selectAllDocsStatus } from './docsSplice';

export const Dashboard = () => {
  const navigate = useNavigate();
  const status = useAppSelector(selectAllDocsStatus);

  const handleClick = (e: any) => {
    navigate('/createDocument');
  };

  const createDocumentButton = () => {
    return (
      <div className='container'>
        <button className='button' onClick={(e) => handleClick(e)}>
          Create a New Doc
        </button>
      </div>
    );
  };

  return (
    <div>
      {createDocumentButton()}
      {status === 'loading' ? <Spinner /> : <DocList />}
    </div>
  );
};
