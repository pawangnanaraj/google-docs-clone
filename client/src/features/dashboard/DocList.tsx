import React from 'react';
import { Button, Grid } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import ArticleIcon from '@mui/icons-material/Article';
import './doclist.css';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import {
  selectAllDocsStatus,
  selectAllDocs,
  allDocumentsAsync,
} from './docsSplice';
import { selectIsAuthenticated } from '../user/authSlice';

export const DocList = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const status = useAppSelector(selectAllDocsStatus);
  const allDocs = useAppSelector(selectAllDocs);
  const isAuthenticated = useAppSelector(selectIsAuthenticated);

  if (isAuthenticated && status === 'idle') {
    dispatch(allDocumentsAsync('a'));
  }

  const handleClick = (documentId: string) => {
    navigate(`/document/${documentId}`);
  };

  const displayDocs = () => {
    return allDocs.map((value) => {
      return (
        <Grid item key={value._id}>
          <Button
            startIcon={<ArticleIcon />}
            style={{ minWidth: 30, fontSize: 20 }}
            variant='outlined'
            defaultValue={value._id}
            onClick={() => handleClick(value._id)}
          >
            {' '}
            Doc {allDocs.indexOf(value) + 1}
          </Button>
        </Grid>
      );
    });
  };

  return (
    <div className='card-container'>
      {' '}
      <Grid container spacing={3}>
        {displayDocs()}
      </Grid>
    </div>
  );
};
