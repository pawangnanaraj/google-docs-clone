import {
  createSlice,
  createAsyncThunk,
  PayloadAction,
  Slice,
} from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import api from '../../utils/api.utils';

export const allDocumentsAsync = createAsyncThunk(
  '/allDocuments',
  async (test: string, thunkAPI) => {
    try {
      const allDocuments = api.get('/document');
      return allDocuments;
    } catch (error: any) {
      const errors = error.response.data;
      return thunkAPI.rejectWithValue(errors);
    }
  }
);

export interface DocumentsState {
  allDocs: [
    {
      _id: string;
      owner: string;
    }
  ];
  isError: Boolean;
  errorMessage: [''] | null;
  status: 'loading' | 'idle' | 'failed' | 'success';
}

const initialState: DocumentsState = {
  allDocs: [
    {
      _id: '',
      owner: '',
    },
  ],
  isError: false,
  errorMessage: null,
  status: 'idle',
};

export const allDocumentsSlice: Slice<DocumentsState> = createSlice({
  name: 'documents',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(allDocumentsAsync.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(
        allDocumentsAsync.rejected,
        (state, action: PayloadAction<any>) => {
          state.status = 'failed';
          state.isError = true;
          // state.errorMessage = action.payload.errors
          //   ? [action.payload.errors]
          //   : null;
        }
      )
      .addCase(
        allDocumentsAsync.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.status = 'success';
          state.isError = false;
          state.allDocs = action.payload.data;
        }
      );
  },
});

export const selectAllDocs = (state: RootState) => state.allDocs.allDocs;
export const selectIsError = (state: RootState) => state.allDocs.isError;
export const selectErrorMessage = (state: RootState) =>
  state.allDocs.errorMessage;
export const selectAllDocsStatus = (state: RootState) => state.allDocs.status;

export default allDocumentsSlice.reducer;
