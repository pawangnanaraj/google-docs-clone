import React from 'react';
import { Link, Navigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { selectIsAuthenticated } from '../user/authSlice';

import { allDocumentsAsync } from '../dashboard/docsSplice';

import './landing.css';

export const Landing = () => {
  const dispatch = useAppDispatch();
  const isAuthenticated = useAppSelector(selectIsAuthenticated);
  if (isAuthenticated) {
    dispatch(allDocumentsAsync('a'));
    return <Navigate to='/dashboard' />;
  }
  return (
    <section className='landing'>
      <div className='dark-overlay'>
        <div className='landing-inner'>
          <p className='x-large'>
            <strong>Collaborative Editor</strong>
          </p>
          <p className='lead'>
            <strong>Pawan's version of Google Docs Clone</strong>
          </p>
          <div className='buttons'>
            <Link to='/register' className='btn btn-primary'>
              <strong>Sign Up</strong>
            </Link>
            <Link to='/login' className='btn btn-light'>
              <strong>Login</strong>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
};
