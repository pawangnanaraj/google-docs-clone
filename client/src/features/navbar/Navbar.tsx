import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import styles from './navbar.module.css';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { selectIsAuthenticated } from '../user/authSlice';
import { logOut } from '../user/authSlice';

const NavBar = () => {
  const dispatch = useAppDispatch();
  const isAuthenticated = useAppSelector(selectIsAuthenticated);

  const onClick = (e: any) => {
    dispatch(logOut(e));
  };

  const guestLinks = '';

  const authLinks = (
    <nav className={styles.navbar}>
      <ul className={styles.navbar_ul}>
        <li>
          <Link className={styles.navbar_li} to='/dashboard'>
            <i className='fas fa-file' /> <span className='hide-sm'>Docs</span>
          </Link>
        </li>
        <li>
          <Link className={styles.navbar_li} onClick={onClick} to='/'>
            <i className='fas fa-sign-out-alt' />{' '}
            <span className='hide-sm'>Logout</span>
          </Link>
        </li>
      </ul>
    </nav>
  );

  return <Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>;
};

export default NavBar;
