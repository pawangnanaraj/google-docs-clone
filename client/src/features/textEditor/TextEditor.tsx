import React, { useCallback, useEffect, Fragment } from 'react';
import { useParams } from 'react-router-dom';
import Quill, { RangeStatic } from 'quill';
import 'quill/dist/quill.snow.css';
import './textEditor.css';
import { io, Socket } from 'socket.io-client';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { selectSocketState, setSocketState } from './setSocketSplice';
import { selectQuillState, setQuillState } from './setQuillSplice';
import * as dotenv from 'dotenv';
import QuillCursors from 'quill-cursors';
import { selectEmail, selectUserName } from '../user/authSlice';
import AddCommentIcon from '@mui/icons-material/AddComment';
import {
  selectTypedComments,
  setCommentsState,
  setAllComments,
  selectAllComments,
} from './commentSplice';

dotenv.config();

// Add sizes to whitelist and register them
const Size = Quill.import('formats/size');
Size.whitelist = ['extra-small', 'small', 'medium', 'large'];

Quill.register(Size, true);

// Add fonts to whitelist and register them
const Font = Quill.import('formats/font');
Font.whitelist = [
  'arial',
  'comic-sans',
  'courier-new',
  'georgia',
  'helvetica',
  'lucida',
];
Quill.register(Font, true);

Quill.register('modules/cursors', QuillCursors);

let cursorsQuill: QuillCursors;
let q: Quill;

export interface DocComment {
  documentId: string;
  user: string;
  comment: {
    range: {
      index: number;
      length: number;
    };
    commentText: string;
  };
  _id?: string;
}

let allComments: DocComment[] = [];

const SAVE_INTERVAL_MS = 600;

export const TextEditor = () => {
  const { id: documentId } = useParams();
  const dispatch = useAppDispatch();
  const email = useAppSelector(selectEmail);
  const name = useAppSelector(selectUserName);
  const commentsState = useAppSelector(selectTypedComments);
  const allCommentsState = useAppSelector(selectAllComments);
  console.log('all comments state = ', allCommentsState);
  const socket: Socket = useAppSelector(selectSocketState);
  const quill = useAppSelector(selectQuillState);

  //------------ debounce function for multicursor ----------------
  const debounce = (func: Function, ms = 30) => {
    let timeout: ReturnType<typeof setTimeout>;
    return function (this: any, ...args: any[]) {
      const context = this;
      const later = function () {
        func.apply(context, args);
      };
      clearTimeout(timeout);
      timeout = setTimeout(later, ms);
    };
  };

  const handleCommentClick = (range: RangeStatic) => {
    q.setSelection(range.index, range.length);
  };

  //------------ viewing comments on the side of the doc -----------
  const viewComments = () => {
    console.log('View All Comments: ', allCommentsState);
    return allCommentsState?.map((value) => {
      if (value.documentId !== documentId) return <div></div>;
      return (
        <div
          className='card m-2 cursor-pointer border border-gray-400 rounded-lg hover:shadow-md hover:border-opacity-0 transform hover:-translate-y-1 transition-all duration-200'
          key={value._id}
          onClick={() => {
            handleCommentClick(value.comment.range);
          }}
          onMouseOver={() => {
            handleCommentClick(value.comment.range);
          }}
        >
          <div className='m-3'>
            <p className='text-lg mb-2'>
              {/* Comment */}{' '}
              <span className='text-sm text-teal-800 font-mono bg-teal-100 inline rounded-full px-2 align-top float-right animate-pulse'>
                {value.user}
              </span>
            </p>
            <p className='font-light font-mono text-sm text-gray-700 hover:text-gray-900 transition-all duration-200'>
              {value.comment.commentText}
              <span>
                {/* <button>
                  {'  '}
                  <HighlightOffIcon color='action' />
                </button> */}
              </span>
            </p>
          </div>
        </div>
      );
      // });
    });
  };

  //-------------- setting the client socket io --------------------
  useEffect(() => {
    const s = io();
    dispatch(setSocketState(s));

    return () => {
      s.disconnect();
    };
  }, [dispatch]);

  //-------------------- Open quill editor -----------------------
  const wrapperRef = useCallback(
    (wrapper) => {
      if (wrapper == null) return;
      wrapper.innerHTML = '';

      const editor = document.createElement('div');
      wrapper.append(editor);

      q = new Quill(editor, {
        theme: 'snow',
        modules: {
          toolbar: '#toolbar',
          cursors: {
            hideDelayMs: 5000,
            hideSpeedMs: 0,
            transformOnTextChange: true,
          },
        },
      });

      cursorsQuill = q.getModule('cursors');

      q.disable();
      q.setText('Please wait while we load the document');

      dispatch(setQuillState(q));
    },
    [dispatch]
  );

  //----------------- loading document based on document Id --------------
  useEffect(() => {
    if (socket == null || quill == null) return;

    socket.once('load-document', (document) => {
      q.setContents(document);
      q.enable();
    });

    socket.emit('get-document', documentId, email);
  }, [socket, quill, documentId, email]);

  // ------------- save the document every ms automatically --------------
  useEffect(() => {
    if (socket == null || quill == null) return;

    const interval = setInterval(() => {
      socket.emit('save-document', q.getContents());
    }, SAVE_INTERVAL_MS);

    return () => {
      clearInterval(interval);
    };
  }, [socket, quill]);

  //---------------------- Multi cursor positions -----------------------
  useEffect(() => {
    const selectionChangeHandler = (cursor: QuillCursors) => {
      // console.log(cursor);
      const updateCursor = (range: any) => {
        setTimeout(() => cursor.moveCursor('cursor', range), 50);
      };

      const debouncedUpdate = debounce(updateCursor, 500);

      return function (range: any, oldRange: any, source: string) {
        if (source === 'user') {
          updateCursor(range);
        } else {
          debouncedUpdate(range);
        }
      };
    };

    // cursorsQuill.createCursor('cursor', 'Alex', 'blue');
    cursorsQuill.createCursor('cursor', name, 'red');

    q.on('selection-change', selectionChangeHandler(cursorsQuill));

    return () => {
      q.off('selection-change', selectionChangeHandler(cursorsQuill));
    };
  }, [quill, name, email]);

  //----------------- loading document based on document Id -----------------
  useEffect(() => {
    if (socket == null || quill == null) return;

    const handler = (delta: any, oldDelta: any, source: string) => {
      if (source !== 'user') return;
      socket.emit('send-changes', delta);
    };
    q.on('text-change', handler);

    return () => {
      q.off('text-change', handler);
    };
  }, [socket, quill]);

  //-------------------- receive changes and update contents --------------
  useEffect(() => {
    if (socket == null || quill == null) return;

    const handler = (delta: any) => {
      q.updateContents(delta);
    };
    socket.on('receive-changes', handler);

    return () => {
      socket.off('receive-changes', handler);
    };
  }, [socket, quill]);

  // // -- Finding the number of clients in a room for creating mulptile quill editors----------
  // useEffect(() => {
  //   // if (socket == null || quill == null) return;
  //   socket.on('number-of-editors', (editors: number) => {
  //     console.log('number of editors : ', editors);
  //   });
  // }, [socket]);

  //-------------------- Adding Comment -------------------------------
  useEffect(() => {
    if (socket == null) return;
    const addCommentButton = document.querySelector('#comment-button');

    const handler = () => {
      const selectionRange = q.getSelection();

      console.log(selectionRange ? selectionRange : 'no selection');

      if (selectionRange === null || selectionRange.length === 0) {
        return alert('Please select text');
      }
      // const text = q.getText(selectionRange.index, selectionRange.length);
      // console.log('User has highlighted: ', text.toString());

      const prompt = window.prompt('Please enter Comment', '');
      // console.log('Comment: ', prompt);

      if (prompt === null || prompt === '') {
        return;
      }
      const comment: DocComment = {
        documentId: documentId ? documentId : '',
        user: email,
        comment: {
          range: { index: selectionRange.index, length: selectionRange.length },
          commentText: prompt,
        },
      };
      console.log(comment);
      dispatch(setCommentsState(comment));
      socket.emit('save-comment', comment);

      q.formatText(selectionRange.index, selectionRange.length, {
        background: '#fff72b',
      });
    };

    addCommentButton?.addEventListener('click', handler);

    return () => {
      addCommentButton?.removeEventListener('click', handler);
    };
  }, [dispatch, commentsState, documentId, email, socket]);

  //-------------------- Loading Comment ------------------------
  useEffect(() => {
    if (socket == null) return;
    const loadComments = (comments: DocComment[]) => {
      allComments = comments;
      dispatch(setAllComments(comments));
      console.log('allComments: ', allComments);
    };

    socket.on('load-comments', loadComments);

    return () => {
      socket.off('load-comments', loadComments);
    };
  }, [socket, dispatch]);

  return (
    <Fragment>
      <div className='contain'>
        <div id='toolbar'>
          <select className='ql-font'>
            <option value='arial' selected>
              Arial
            </option>
            <option value='comic-sans'>Comic Sans</option>
            <option value='courier-new'>Courier New</option>
            <option value='georgia'>Georgia</option>
            <option value='helvetica'>Helvetica</option>
            <option value='lucida'>Lucida</option>
          </select>
          <select className='ql-size'>
            <option value='extra-small'>13</option>
            <option value='small'>14</option>
            <option value='medium' selected>
              18
            </option>
            <option value='large'>20</option>
          </select>

          <select className='ql-align' />
          <select className='ql-color' title='Font Color' />
          <select className='ql-background' title='Background Color' />
          <button className='ql-clean' title='Remove Formating' />
          <button className='ql-image' title='Insert Image' />
          <button className='ql-list' value='ordered' title='Numbered List' />
          <button className='ql-list' value='bullet' title='Bullet List' />
          <button className='ql-script' value='sub' title='Sub Script' />
          <button className='ql-script' value='super' title='Super Script' />
          <button className='ql-bold' title='Bold' />
          <button className='ql-italic' title='Italic' />
          <button className='ql-underline' title='Underline' />
          <button className='ql-code-block' title='Code Block' />
          <button className='ql-blockquote' title='Quote' />
          <button className='ql-link' title='Insert Link' />
          <button
            className='addComment'
            id='comment-button'
            title='Add Comment'
          >
            <AddCommentIcon />
          </button>
        </div>
        <div className='grid grid-cols-9 gap-0 justify-items-center'>
          <div className='col-start-3 col-end-8 mt-10' ref={wrapperRef}></div>
          <div className='col-start-8 col-span-2 mt-16'>
            {' '}
            <div>
              <h1> {''}</h1>
            </div>
            {viewComments()}
            <div id='comments'>{/* <li>TTest</li> */}</div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
