import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { DocComment } from './TextEditor';

interface Comment {
  typedComment: {
    range: {
      index: number;
      length: number;
    };
    commentText: string;
    user: string;
  };
  allComments: DocComment[] | null;
}

const initialState: Comment = {
  typedComment: {
    range: {
      index: 0,
      length: 0,
    },
    commentText: '',
    user: '',
  },
  allComments: null,
};

export const allCommentsState: Slice<Comment> = createSlice({
  name: 'comments',
  initialState,
  reducers: {
    setCommentsState: (state, action: PayloadAction<any>) => {
      state.typedComment = action.payload;
      console.log('payload: ', action.payload);
      console.log('state typed Comment: ', state.typedComment);
    },
    setAllComments: (state, action: PayloadAction<any>) => {
      state.allComments = action.payload;
    },
  },
  extraReducers: {},
});

export const selectTypedComments = (state: RootState) =>
  state.allComments.typedComment;
export const selectAllComments = (state: RootState) =>
  state.allComments.allComments;

export const { setCommentsState, setAllComments } = allCommentsState.actions;

export default allCommentsState.reducer;
