import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface QuillState {
  quill: any;
}

const initialState: QuillState = {
  quill: null,
};

export const viewQuillState: Slice<QuillState> = createSlice({
  name: 'sockets',
  initialState,
  reducers: {
    setQuillState: (state, action: PayloadAction<any>) => {
      state.quill = action.payload;
    },
  },
  extraReducers: {},
});

export const selectQuillState = (state: RootState) => state.quill.quill;

export const { setQuillState } = viewQuillState.actions;

export default viewQuillState.reducer;
