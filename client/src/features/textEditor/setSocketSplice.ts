import { createSlice, PayloadAction, Slice } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface SocketState {
  socket: any;
}

const initialState: SocketState = {
  socket: null,
};

export const viewSocketState: Slice<SocketState> = createSlice({
  name: 'sockets',
  initialState,
  reducers: {
    setSocketState: (state, action: PayloadAction<any>) => {
      state.socket = action.payload;
    },
  },
  extraReducers: {},
});

export const selectSocketState = (state: RootState) => state.socket.socket;

export const { setSocketState } = viewSocketState.actions;

export default viewSocketState.reducer;
