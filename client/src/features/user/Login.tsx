import React, { Fragment, useState } from 'react';
import { Link, Navigate } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye } from '@fortawesome/free-solid-svg-icons';
import styles from './user.module.css';
import {
  loginAsync,
  selectEmail,
  selectPassword,
  selectloginRegAuth,
  selectErrorMessage,
} from './authSlice';
import { setEmail, setPassword } from './authSlice';
import { Alert } from '@mui/material';
import { allDocumentsAsync } from '../dashboard/docsSplice';

const eye = <FontAwesomeIcon icon={faEye} />;

const LoginUser = () => {
  const dispatch = useAppDispatch();
  const email = useAppSelector(selectEmail);
  const password = useAppSelector(selectPassword);
  const isAuthenticated = useAppSelector(selectloginRegAuth);
  const errorMessage = useAppSelector(selectErrorMessage);

  const onChangeEmail = (e: any) => {
    dispatch(setEmail(e.target.value));
  };

  const onChangePassword = (e: any) => {
    dispatch(setPassword(e.target.value));
  };

  const onSubmit = (e: any) => {
    e.preventDefault();
    const body = { email, password };
    dispatch(loginAsync(body));
  };

  const [passwordShown, setPasswordShown] = useState(false);

  const togglePasswordVisibility = () => {
    setPasswordShown(passwordShown ? false : true);
  };

  if (isAuthenticated) {
    dispatch(allDocumentsAsync('a'));
    return <Navigate to='/dashboard' />;
  }

  const ErrorAlertFragment = errorMessage ? (
    <Fragment>
      <Alert severity='error'>
        <strong>
          {errorMessage[0][0]}{' '}
          {errorMessage[0][1] ? `and ${errorMessage[0][1]}` : ''}
        </strong>
      </Alert>
    </Fragment>
  ) : (
    ''
  );

  return (
    <section className={styles.container}>
      {ErrorAlertFragment}
      <h1 className={styles.large}>Log In</h1>
      <p className={styles.lead}>
        <i className='fas fa-user' /> Please log in to start collaborating
      </p>
      <form className={styles.form} onSubmit={onSubmit}>
        <div className={styles.form_group}>
          <input
            className={styles.form_textarea}
            type='email'
            placeholder='Email Address'
            name='email'
            value={email}
            onChange={onChangeEmail}
            // required
          />
        </div>
        <div className={styles.form_group}>
          <input
            type={passwordShown ? 'text' : 'password'}
            placeholder='Password'
            name='password'
            value={password}
            onInput={onChangePassword}
            // minLength = '6'
          />
          <i
            className={styles.form_textarea_i}
            onClick={togglePasswordVisibility}
          >
            {eye}
          </i>
        </div>

        {/* <input type='submit' className={styles.buttn_primary} value='Sign In' /> */}
        <button
          className='bg-green-700 hover:bg-green-750 text-white font-bold py-2 px-4 rounded'
          type='submit'
          value={'Sign In'}
        >
          Sign In
        </button>
      </form>
      <p className='my-1'>
        Don't have an account? <Link to='/register'>Register</Link>
      </p>
    </section>
  );
};

export default LoginUser;
