import React, { useEffect } from 'react';
import { Navigate, Outlet, RouteProps } from 'react-router-dom';
import { useAppSelector, useAppDispatch } from '../app/hooks';
import { selectIsAuthenticated, authStatus } from '../features/user/authSlice';
import setAuthToken from '../utils/setAuthToken';
import { authAsync } from '../features/user/authSlice';
import Spinner from './Spinner';

export const PrivateRoute = (props: RouteProps) => {
  const dispatch = useAppDispatch();
  useEffect(() => {
    if (localStorage.token) {
      setAuthToken(localStorage.token);
    }
    dispatch(authAsync(localStorage.token));

    // eslint-disable-next-line
  }, []);
  const isAuthenticated = useAppSelector(selectIsAuthenticated);
  const isLoading = useAppSelector(authStatus);

  return isLoading === 'loading' ? (
    <Spinner />
  ) : isAuthenticated ? (
    <Outlet />
  ) : (
    <Navigate to='/login' />
  );
};
