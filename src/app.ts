import express, { Application, Request, Response, NextFunction } from 'express';
import documentService from './services/document.service';
import * as dotenv from 'dotenv';
import { connectDB } from './helpers/connectMongoDB';
import cors from 'cors';
import path from 'path';
import { DocumentModel } from './models/Document.model';
import { registerUserRouter } from './router/api/user.router';
import { authRouter } from './router/api/auth.router';
import { documentRouter } from './router/api/document.router';

import { createServer } from 'http';
import { Server } from 'socket.io';

dotenv.config();

//initialise our app
const app: Application = express();

connectDB();
app.use(cors());
app.use(express.json());

const httpServer = createServer(app);
export const io = new Server(httpServer, {
  cors: {
    // origin: 'http://localhost:3000' || '/',
    // methods: ['GET', 'POST'],
  },
});

app.use('/api/register', registerUserRouter);
app.use('/api/auth', authRouter);
app.use('/api/document', documentRouter);

app.use(function (error: any, req: Request, res: Response, next: NextFunction) {
  console.log(error.message);
  res
    .status(error.status || error.statusCode || 500)
    .json({ errors: [error.message || 'server Error'] });
});

app.get('/ping', (req: Request, res: Response) => {
  return res.send('pong');
});

//Serve static assests in production
if (process.env.NODE_ENV === 'production') {
  //Set static folder

  app.use(express.static('client/build'));
  console.log('server running');
  app.get('*', (req: Request, res: Response) => {
    res.sendFile(path.resolve('/app/client/build/index.html'));
  });
}

const PORT = process.env.PORT || 9000;
httpServer.listen(PORT, () => console.log(`server running ${PORT} `));

io.on('connection', (socket) => {
  console.log('Client connected');
  socket.on('get-document', async (documentId, email) => {
    const document = await documentService.findOrCreateDocument(
      documentId,
      email
    );
    const comments = await documentService.findAllComments(documentId);

    console.log('server comments: ', comments);

    socket.join(documentId);
    socket.emit('load-document', document?.data);
    if (comments.length > 0) {
      socket.emit('load-comments', comments);
    }

    socket.on('send-changes', (delta) => {
      socket.broadcast.to(documentId).emit('receive-changes', delta);
      // console.log(delta);
      // console.log(io.sockets.adapter.rooms);
    });
    if (io.sockets.adapter.rooms.get(documentId)) {
      const editors = io.sockets.adapter.rooms.get(documentId)?.size;
      if (editors) {
        socket.emit('number-of-editors', editors);
      }
    }
    // const editors = io.sockets.adapter.rooms.get(documentId)?.size;

    // console.log('number of connections', io.engine.clientsCount);
    console.log(
      'number of connections: ',
      io.sockets.adapter.rooms.get(documentId)?.size
    );

    socket.on('cursor-changes', (cursorsQuill) => {
      socket.broadcast
        .to(documentId)
        .emit('receive-cursor-changes', cursorsQuill);
      // console.log(cursorsQuill);
    });

    socket.on('save-document', async (data) => {
      await DocumentModel.findByIdAndUpdate(documentId, { data: data });
    });

    socket.on('save-comment', async (data) => {
      await documentService.saveComment(data);
      const comments = await documentService.findAllComments(documentId);
      socket.broadcast.to(documentId).emit('load-comments', comments);
      socket.emit('load-comments', comments);
    });
  });

  socket.on('disconnect', () => console.log('Client disconnected'));
});

export default app;
