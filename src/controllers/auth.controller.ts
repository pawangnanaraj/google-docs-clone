import { Request, Response } from 'express';
import authService from '../services/auth.service';

//----------------------------SignIn Controller------------------------------
const signIn = async (req: Request, res: Response) => {
  //see if the user exits, send an error
  const { email, password } = req.body;
  //   console.log(`email: ${email}, password: ${password}`);
  const token = await authService.signIn(email, password);
  res.json({ token: token });
};

//----------------------------Authenticate Controller------------------------

const authenticate = async (req: Request, res: Response) => {
  //get only id and leave the password
  const id: string = req.body.user.user.id;
  // console.log(`_id: ${id}`);

  const userId = await authService.authenticate(id);
  // console.log(`userId: ${userId}`);

  res.json(userId);
};

//----------------------------Export Auth Controller--------------------------
export default {
  signIn,
  authenticate,
};
