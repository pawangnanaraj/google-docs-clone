import { Request, Response } from 'express';
import documentService from '../services/document.service';

const findAllDocuments = async (req: Request, res: Response) => {
  const documents = await documentService.findAllDocuments();

  res.json(documents);
};

const findAllComments = async (req: Request, res: Response) => {
  const documentId = req.params.id;
  const comments = await documentService.findAllComments(documentId);

  res.json(comments);
};

export default {
  findAllDocuments,
  findAllComments,
};
