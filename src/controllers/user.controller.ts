import { Request, Response } from 'express';
import userService from '../services/user.service';

let isAgent: boolean;

// ---------------Register user and agent --------------------------

const registerUser = async (req: Request, res: Response) => {
  const { userName, email, password } = req.body;

  const token = await userService.registerUser(userName, email, password);

  res.json({ token: token });
};

export default {
  registerUser,
};
