import Joi from 'joi';

//-----------Register Validation -----------
export const registerValidation = Joi.object().keys({
  userName: Joi.string().lowercase().trim().min(3).required(),
  email: Joi.string().lowercase().trim().email().required(),
  password: Joi.string().alphanum().min(6).max(8).required(),
});

//-----------Login Validation -------------
export const loginValidation = Joi.object().keys({
  email: Joi.string().lowercase().trim().email().required(),
  password: Joi.string().alphanum().min(6).max(8).required(),
});
