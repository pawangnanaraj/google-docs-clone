export default interface Comment {
  documentId: string;
  user: string;
  comment: {
    range: {
      index: number;
      length: number;
    };
    commentText: string;
  };
  parentID: string;
}
