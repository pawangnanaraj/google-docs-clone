export default interface Document {
  _id: string;
  data: object;
  owner: string;
  //   comments: [string];
  //   editors: [string];
  //   viewers: [string];
}
