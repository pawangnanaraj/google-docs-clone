export default interface User {
  userName: string;
  email: string;
  password: string;
  _id?: string;
  //date: number;
}
