import * as jwt from 'jsonwebtoken';
import { Response, Request, NextFunction } from 'express';
import * as dotenv from 'dotenv';

dotenv.config();

if (!process.env.JWT_KEY) {
  process.exit(1);
}
const jwtKey = process.env.JWT_KEY;

export const authMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  //get token from header
  const token = req.header('x-auth-token');
  // console.log(`token: ${token}`);
  //check if no token
  if (token === undefined || null) {
    return res.status(401).json({ msg: 'No token, authotization denied' });
  }

  //if there is a token, verify the token
  try {
    // console.log('trying to decode');
    const decoded = jwt.verify(token, jwtKey);
    req.body.user = decoded;
    // console.info(decoded);
    next();
  } catch (err: any) {
    res.status(401).json({ msg: 'Token is not valid' });
  }
};
