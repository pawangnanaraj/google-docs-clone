import httpStatusCodes from './errorCodes';

export class UserAlreadyAvailableError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.BAD_REQUEST
  ) {
    super(message);
    this.name = 'UserAlreadyAvailableError';
  }
}

export class NotFoundError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.NOT_FOUND
  ) {
    super(message);
    this.name = 'notFoundError';
  }
}

export class NotMatchedError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.UNAUTHORISED_USER
  ) {
    super(message);
    this.name = 'NotMatchError';
  }
}

export class UnauthorizedUserError extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.UNAUTHORISED_USER
  ) {
    super(message);
    this.name = 'unauthorizedUserError';
  }
}

export class UnableToLoadDocuments extends Error {
  constructor(
    public message: string,
    public status = httpStatusCodes.INTERNAL_SERVER
  ) {
    super(message);
    this.name = 'unableToLoadDocuments';
  }
}
