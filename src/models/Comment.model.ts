import { Schema, model } from 'mongoose';
import Comment from '../interfaces/comment.interface';

const CommentSchema = new Schema<Comment>({
  documentId: {
    type: String,
    required: true,
  },
  user: {
    type: String,
    required: true,
  },
  comment: {
    range: {
      index: {
        type: Number,
        required: true,
      },
      length: {
        type: Number,
        required: true,
      },
    },
    commentText: {
      type: String,
      required: true,
    },
  },
  parentID: {
    type: String,
    default: '',
  },
});

export const CommentModel = model<Comment>('Comment', CommentSchema);
