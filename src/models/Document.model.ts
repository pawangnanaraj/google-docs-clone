import { Schema, model } from 'mongoose';
import Document from '../interfaces/document.interface';

const DocumentSchema = new Schema<Document>({
  _id: {
    type: String,
    required: true,
  },
  data: {
    type: Object,
  },
  owner: {
    type: String,
    required: true,
  },
  //   comments: {
  //     type: [String],
  //     default: [''],
  //   },
  //   editors: {
  //     type: [String],

  //     default: [''],
  //   },
  //   viewers: {
  //     type: [String],

  //     default: [''],
  //   },
});

export const DocumentModel = model<Document>('Document', DocumentSchema);
