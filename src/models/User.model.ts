import { Schema, model } from 'mongoose';
import User from '../interfaces/user.interface';

const UserSchema = new Schema<User>(
  {
    userName: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
  },
  { timestamps: true }
);

export const UserModel = model<User>('User', UserSchema);
