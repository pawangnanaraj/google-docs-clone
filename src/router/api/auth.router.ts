import express from 'express';
import authCtrl from '../../controllers/auth.controller';
import asyncWrapper from '../../helpers/asyncWrapper';
import { validationMiddleware } from '../../middleware/validation.middleware';
import { loginValidation } from '../../helpers/customValidation';
import { authMiddleware } from '../../middleware/auth.middleware';

export const authRouter = express.Router();

// route --> POST  api/register
//authenticate user
authRouter.get('/', authMiddleware, asyncWrapper(authCtrl.authenticate));

// route --> POST  api/auth/
//login
authRouter.post(
  '/',
  validationMiddleware(loginValidation),
  asyncWrapper(authCtrl.signIn)
);
