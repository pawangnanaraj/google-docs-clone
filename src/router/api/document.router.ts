import express from 'express';
import documentCtrl from '../../controllers/document.controller';
import asyncWrapper from '../../helpers/asyncWrapper';
import { authMiddleware } from '../../middleware/auth.middleware';

export const documentRouter = express.Router();

// route --> GET api/document
// find all the documents
documentRouter.get(
  '/',
  authMiddleware,
  asyncWrapper(documentCtrl.findAllDocuments)
);

documentRouter.get(
  '/:id/comments',
  authMiddleware,
  asyncWrapper(documentCtrl.findAllComments)
);
