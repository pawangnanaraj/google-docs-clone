import express from 'express';
import userCtrl from '../../controllers/user.controller';

import asyncWrapper from '../../helpers/asyncWrapper';
import { validationMiddleware } from '../../middleware/validation.middleware';
import { registerValidation } from '../../helpers/customValidation';

export const registerUserRouter = express.Router();

// route --> POST  api/register
//register user

registerUserRouter.post(
  '/',
  validationMiddleware(registerValidation),
  asyncWrapper(userCtrl.registerUser)
);
