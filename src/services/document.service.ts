import Comment from '../interfaces/comment.interface';
import { CommentModel } from '../models/Comment.model';
import { DocumentModel } from '../models/Document.model';

//------------------- Find or Create Document ---------------------------
const findOrCreateDocument = async (documentId: string, email: string) => {
  if (documentId == null) return;
  console.log('email: ', email);

  const document = await DocumentModel.findById(documentId);

  if (document) return document;
  console.log('creating new document');

  return await DocumentModel.create({
    _id: documentId,
    owner: email,
    data: ' ',
  });
};

//----------------- Find all documents -------------------------------
const findAllDocuments = async () => {
  const documents = await DocumentModel.find({}).select('owner');

  return documents;
};

//-------------------- Save a Comment ---------------------------------
const saveComment = async (comment: Comment) => {
  const newComment = new CommentModel(comment);

  return await CommentModel.create(newComment);
};

//--------------- Find all Comments in a document ---------------------

const findAllComments = async (documentId: string) => {
  const comments = await CommentModel.find({ documentId: documentId });

  return comments;
};

export default {
  findOrCreateDocument,
  findAllDocuments,
  saveComment,
  findAllComments,
};
