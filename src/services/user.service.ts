import { UserModel } from '../models/User.model';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { UserAlreadyAvailableError } from '../middleware/customErrors';
import * as dotenv from 'dotenv';

dotenv.config();

if (!process.env.JWT_KEY) {
  process.exit(1);
}
const jwtKey = process.env.JWT_KEY;

//----------------------------- Register User ---------------------------------
const registerUser = async (
  userName: string,
  email: string,
  password: string
) => {
  const users = await UserModel.find({ email: email, userName: userName });

  if (users.length > 0) {
    throw new UserAlreadyAvailableError('user already exists');
  } else {
    const user = new UserModel({
      userName,
      email,
      password,
    });

    //encrypt the password
    const salt = await bcrypt.genSalt(10);

    user.password = await bcrypt.hash(password, salt);

    await user.save();

    //Generate the token
    const payload = {
      user: {
        id: user.id,
      },
    };

    const token = jwt.sign(payload, jwtKey, { expiresIn: 36000 });

    return token;
  }
};

export default {
  registerUser,
};
